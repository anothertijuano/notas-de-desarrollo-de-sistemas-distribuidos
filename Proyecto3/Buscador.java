import java.io.*;

public class Buscador {
    
    public static int buscar(FileReader reader, String palabra) throws IOException {
        BufferedReader buffer = new BufferedReader(reader);
        String[] palabras = null;  
        String str;     
        // Contador
        int cont = 0;    
        // Read the contents of the file
        while((str = buffer.readLine()) != null) {
            // Split the word using space
            palabras = str.split("[¿?!¡.,;:- ]");  
            for (String word : palabras) {
                //Search for the word
                if (word.equals(palabra.toLowerCase())) {
                // If present, increment the counter
                cont++;    
                }
            }
        }
        if(cont!=0) {
            System.out.println("The word is present "+ cont + " times in the file");
        }
        else {
            System.out.println("The word doesn't exist in the file!");
        } 
        reader.close();
        return(cont);
    }

}