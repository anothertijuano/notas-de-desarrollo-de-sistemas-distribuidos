import java.util.*;
class main5 {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        Random r = new Random();
        byte[] cadenota = new byte[n*4];
        
        Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
        String s = "";
        
        //long t0 = System.nanoTime();
        for (int i=0; i<(n*4); i=i+4) {
            cadenota[i] = (byte)(65 + r.nextInt(26));
            cadenota[i+1] = (byte)(65 + r.nextInt(26));
            cadenota[i+2] = (byte)(65 + r.nextInt(26));
            cadenota[i+3] = (byte) ' ';
            s = ""+cadenota[i]+cadenota[i+1]+cadenota[i+2];
            if(ht.containsKey(s)) {
                int x = ht.get(s);
                x = x+1;
                ht.put(s, x);
            } else {
                ht.put(s, 1);
            }
        }
        //long t1 = System.nanoTime();
        //long tiempo = t1 - t0;
        //System.out.println("Nanosegundos: " + tiempo);
        System.out.println("Número de palabras únicas: "+ht.size());
        int sum = 0;
        Enumeration<String> e = ht.keys();
        while (e.hasMoreElements()) {
            String key = e.nextElement();
            sum = sum+ht.get(key);
        }
        System.out.println("Número de veces que se repite cada palabra (promedio): " + sum/ht.size());
    }
}

