public class ejercicio4 {
    private static int fibi(int i) {
        if(i==0)
            return 0;
        if(i==1)
            return 1;
        if(i==2)
            return 1;
        return(fibi(i-1)+fibi(i-2)+fibi(i-3));
    }

    public static void main(String[] args) {
        for (int n=0; n<20; n++) {
            System.out.printf("%d\n", fibi(n));
        }
    }
}
