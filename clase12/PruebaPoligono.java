import java.util.Random;

public class PruebaPoligono {

    public static void main (String[] args) {

        int n = Integer.parseInt(args[0]);
        Random rand = new Random();
        double vert[] = new double[2*n];

        int minimum = -100;
        int maximum = 100;

        long t0 = System.nanoTime();
        for(int i=0; i<(2*n); i=i+2) {
            vert[i] = rand.nextDouble()* (maximum - minimum) + minimum;;
            vert[i+1] = rand.nextDouble();
        }

        PoligonoIrreg pol1 = new PoligonoIrreg(n, vert);
        long t1 = System.nanoTime();
        long tiempo = t1 - t0;
        //System.out.println("Nanosegundos: " + tiempo);
        System.out.println("\nDesordenados:\n");
        System.out.println(pol1);
        pol1.ordenaVertices();
        System.out.println("\nOrdenados:\n");
        System.out.println(pol1);


    }

}

