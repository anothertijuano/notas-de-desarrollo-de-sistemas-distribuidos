# Comunicación en un sistema distribuido

## TCP vs UDP

En el video se hace uso de TCP/IP por la fiabilidad del mismo.


## Transacción HTTP

El protocolo http se conforma de un mensaje de solicitud que viaja de cliente a servidor y un mensaje de respuesta que viaja de servidor a cliente, aunque esté vacío.

Se cuenta con un método, una ruta relativa, la versión del protocolo, headers y cuerpo.

En el método get se mandan los parámetros como parte de la URL 

## HTTP1 vs HTTP2

En http1 las solicitudes se mandan y atienden de manera lineal.

En http2 se pueden mandar multiples solicitudes al mismo tiempo.

La respuesta http viene acompañado de un status code que indica el tipo de resultado que se obtiene de la operación.
