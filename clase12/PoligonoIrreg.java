import java.util.*;

public class PoligonoIrreg{

    // ArrayLis de objetos Coordenada
    List<Coordenada> vertices = new ArrayList<Coordenada>();
    

    public PoligonoIrreg(int n, double[] verti){
        for(int i=0; i<(2*n); i=i+2) {
            // Setter
            anadeVertice(new Coordenada(verti[i], verti[i+1]));
        }
    }



    //Método setter de la coordenada superior izquierda
    public void anadeVertice(Coordenada x) {
        vertices.add(x);        
    }


    //Método para organizar los verices de menor a mayoren función de su magnitud.
    public void ordenaVertices() {
        Collections.sort(vertices);
    }


    // Modificamos to string para imprimir verices en lugar de retornar string
    @Override
    public String toString() {
        String str="";
        vertices.forEach(System.out::println);
        return str;
    }

}

