/*

   Servidor http en java haciendo uso de las librerías estandar
   cuenta con dos endpoints:
   GET /status
        Regresa el mensaje "Estoy vivo"
    
   POST /task
        Realiza multiplicaciones con varios factores, el servidor decuelve el resultado de multiplicar dichos factores.

*/




/*

 *  MIT License

 *

 *  Copyright (c) 2019 Michael Pogrebinsky - Distributed Systems & Cloud Computing with Java

 *

 *  Permission is hereby granted, free of charge, to any person obtaining a copy

 *  of this software and associated documentation files (the "Software"), to deal

 *  in the Software without restriction, including without limitation the rights

 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell

 *  copies of the Software, and to permit persons to whom the Software is

 *  furnished to do so, subject to the following conditions:

 *

 *  The above copyright notice and this permission notice shall be included in all

 *  copies or substantial portions of the Software.

 *

 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR

 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,

 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE

 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER

 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,

 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

 *  SOFTWARE.

 */



import com.sun.net.httpserver.Headers;

import com.sun.net.httpserver.HttpContext;

import com.sun.net.httpserver.HttpExchange;

import com.sun.net.httpserver.HttpServer;



import java.io.IOException;

import java.io.OutputStream;

import java.math.BigInteger;

import java.net.InetSocketAddress;

import java.util.Arrays;

import java.util.concurrent.Executors;



public class WebServer {

	//Se definen ambos endpoints
    private static final String TASK_ENDPOINT = "/task"; 

    private static final String STATUS_ENDPOINT = "/status";



    private final int port;

    private HttpServer server;



    public static void main(String[] args) {
		// Se establece el puerto
        int serverPort = 8080;

        if (args.length == 1) {

            serverPort = Integer.parseInt(args[0]);

        }


		// Se instancía un objeto WebServer con el puerto definido
        WebServer webServer = new WebServer(serverPort);

		// Se inicia el server
        webServer.startServer();



        System.out.println("Servidor escuchando en el puerto " + serverPort);

    }



    public WebServer(int port) {
		//Se establece el puerto
        this.port = port;

    }



    public void startServer() {

        try {

            this.server = HttpServer.create(new InetSocketAddress(port), 0);

        } catch (IOException e) {

            e.printStackTrace();

            return;

        }


		//Se necesita un listener para cada para cada endpoint 
        HttpContext statusContext = server.createContext(STATUS_ENDPOINT);

        HttpContext taskContext = server.createContext(TASK_ENDPOINT);



        statusContext.setHandler(this::handleStatusCheckRequest);

        taskContext.setHandler(this::handleTaskRequest);


		// Se crea un pool de threads 
        server.setExecutor(Executors.newFixedThreadPool(8));

		// Inicia la operación del server
        server.start();

    }



	// Este metodo es el que atiende las peticiones al endpoint task
    private void handleTaskRequest(HttpExchange exchange) throws IOException {
		// Se valida que el metodo sea POST
        if (!exchange.getRequestMethod().equalsIgnoreCase("post")) {

            exchange.close();

            return;

        }


		//Se establecen los headers
        Headers headers = exchange.getRequestHeaders();
		
        if (headers.containsKey("X-Test") && headers.get("X-Test").get(0).equalsIgnoreCase("true")) {

            String dummyResponse = "123\n";

            sendResponse(dummyResponse.getBytes(), exchange);

            return;

        }



        boolean isDebugMode = false;

        if (headers.containsKey("X-Debug") && headers.get("X-Debug").get(0).equalsIgnoreCase("true")) {

            isDebugMode = true;

        }



        long startTime = System.nanoTime();



        byte[] requestBytes = exchange.getRequestBody().readAllBytes();

        byte[] responseBytes = calculateResponse(requestBytes);



        long finishTime = System.nanoTime();



        if (isDebugMode) {

            String debugMessage = String.format("La operación tomó %d nanosegundos", finishTime - startTime);

            exchange.getResponseHeaders().put("X-Debug-Info", Arrays.asList(debugMessage));

        }



        sendResponse(responseBytes, exchange);

    }


	// Esta es la función en la que realiza la operación
    private byte[] calculateResponse(byte[] requestBytes) {

        String bodyString = new String(requestBytes);

        String[] stringNumbers = bodyString.split(",");



        BigInteger result = BigInteger.ONE;



        for (String number : stringNumbers) {

            BigInteger bigInteger = new BigInteger(number);

            result = result.multiply(bigInteger);

        }



        return String.format("El resultado de la multiplicación es %s\n", result).getBytes();

    }


	// Aquí se atiende la petición status
    private void handleStatusCheckRequest(HttpExchange exchange) throws IOException {

		// Se valida que el metodo sea GET
        if (!exchange.getRequestMethod().equalsIgnoreCase("get")) {

            exchange.close();

            return;

        }



        String responseMessage = "El servidor está vivo\n";

        sendResponse(responseMessage.getBytes(), exchange);

    }


	// Se manda la respuesta
    private void sendResponse(byte[] responseBytes, HttpExchange exchange) throws IOException {
		//Notese que se manda un 200 porque la función se ejecuta con éxito
        exchange.sendResponseHeaders(200, responseBytes.length);

        OutputStream outputStream = exchange.getResponseBody();

        outputStream.write(responseBytes);

        outputStream.flush();

        outputStream.close();

        exchange.close();

    }

}


