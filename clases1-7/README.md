# Ejercicios de programación

### 1

Vease clase8



### 2

Elabore una línea de código que imprima con printf el valor de Math.PI con 10 dígitos decimales

```
public class ejercicio2 {
    public static void main(String[] args) {
        System.out.printf("%.10f\n", Math.PI);
    }
}
```


### 3

Escriba un programa que cuente del 1 al 15, imprima cada número y luego cuente hacia atrás de dos en dos hasta el 1, imprimiendo nuevamente cada número.

```
public class ejercicio3 {
    public static void main(String[] args) {
        int start = 1;
        int end = 15;
        int steps = 2;

        for (int i=start; i<=end; i++) {
            System.out.printf("%d\n", i);
        }

        for (int i=end; i>=start; i=i-steps) {
            System.out.printf("%d\n", i);
        }
    }
}
```



### 4

Cada término de la serie de Fibonacci se forma sumando los dos términos anteriores. Elabore un programa similar que cree una serie sumando los tres términos anteriores. El programa deberá imprimir los primeros 20 términos de esta serie.

```
public class ejercicio4 {
    private static int fibi(int i) {
        if(i==0)
            return 0;
        if(i==1)
            return 1;
        if(i==2)
            return 1;
        return(fibi(i-1)+fibi(i-2)+fibi(i-3));
    }

    public static void main(String[] args) {
        for (int n=0; n<20; n++) {
            System.out.printf("%d\n", fibi(n));
        }
    }
}
```



