// Proyecto 2
// David Andrade Olvera
// 4CM11

import java.awt.geom.Path2D;

public class PoliShape extends Path2D.Double {
    public PoliShape(PoligonoReg poligono) {
        //System.out.println(poligono);
        Coordenada[] points = new Coordenada[poligono.getNvertices()];
        points = poligono.coordenadas();
        moveTo(points[0].abcisa(), points[0].ordenada());
        for(int i=1; i<points.length; i++) {
            lineTo(points[i].abcisa(), points[i].ordenada());
        }
        closePath();
    }
} 