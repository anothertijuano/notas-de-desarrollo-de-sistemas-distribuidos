import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

// Read the full article https://dev.to/mateuszjarzyna/build-your-own-http-server-in-java-in-less-than-one-hour-only-get-method-2k02
public class Server {

    // Función principal
    public static void main( String[] args ) throws Exception {
        // Primer argumento es puerto
        int port = Integer.parseInt(args[0]);

        // Segundo argumento es el archivo
        File file = new File(args[1]); 
        FileReader fr = new FileReader(file);

        // Creamos ServerSocket y esperamos nuevas conexiones
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                try (Socket client = serverSocket.accept()) {
                    // Atendemos petición
                    atenderCliente(client, fr);
                }
            }
        }
    }

    private static int atenderCliente(Socket client, FileReader fr) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));

        StringBuilder requestBuilder = new StringBuilder();
        String line;
        while (!(line = br.readLine()).isBlank()) {
            requestBuilder.append(line + "\r\n");
        }

        String request = requestBuilder.toString();
        String[] requestsLines = request.split("\r\n");
        String[] requestLine = requestsLines[0].split(" ");
        String method = requestLine[0];
        String path = requestLine[1];
        String host = requestsLines[1].split(" ")[1];

        List<String> headers = new ArrayList<>();
        for (int h = 2; h < requestsLines.length; h++) {
            String header = requestsLines[h];
            headers.add(header);
        }

        String[] URL = path.split("\\?");

        if(!method.equals("GET")) {
            System.out.println(String.format("Not %s.", method));
            sendResponse(client, "405 Method Not Allowed", "text/html; charset=UTF-8", "Method Not Allowed".getBytes());
            return(405);
        }

        if(!URL[0].equals("/busqueda")) {
            System.out.println("Not path");
            sendResponse(client, "404 Not Found", "text/html; charset=UTF-8", "Not Found".getBytes());
            return(404);
        }

        String accessLog = String.format("Host: %s\nMethod: %s\nPath: %s\n", host, method, URL[0]);
        System.out.println(accessLog);

        String[] txtArgs = URL[1].split("\\&");
        String[] args = new String[txtArgs.length];
        for(int i=0; i<txtArgs.length; i++) {
            args[i] = txtArgs[i].split("=")[1];
        }
        for(Object s: args) {
            System.out.println(s);
        }

        // Buscar
        int[] n = buscar(fr, args);
        String json = new String();
        json += "{";
        for(int i=0; i<n.length; i++) {
            json += String.format("{\"%s\": %d},", args[i], n[i]);
        }
        json += "}";

        sendResponse(client, "200 OK", "application/json", json.getBytes());
        return(200);
        
    }

    private static void sendResponse(Socket client, String status, String contentType, byte[] content) throws IOException {
        OutputStream clientOutput = client.getOutputStream();
        clientOutput.write(("HTTP/1.1 " + status).getBytes());
        clientOutput.write(("ContentType: " + contentType + "\r\n").getBytes());
        clientOutput.write("\r\n".getBytes());
        clientOutput.write(content);
        clientOutput.write("\r\n\r\n".getBytes());
        clientOutput.flush();
        client.close();
    }

    public static int[] buscar(FileReader reader, String[] buscar) throws IOException {
        BufferedReader buffer = new BufferedReader(reader);
        String[] palabras = null;  
        String str;     
        // Contador
        int[] cont = new int[buscar.length];    
        // Read the contents of the file
        while((str = buffer.readLine()) != null) {
            // Split the word using space
            palabras = str.split(" ");  
            for (int i=0;i<palabras.length; i++) {
                String word = palabras[i];
                for(int j=0; j<buscar.length; j++) {
                    word = word.replace(",", ""); 
                    word = word.replace(".", ""); 
                    word = word.replace(":", ""); 
                    word = word.replace(";", ""); 
                    word = word.replace("\"", ""); 
                    //Search for the word
                    if (word.toLowerCase().equals(buscar[j].toLowerCase())) {
                    // If present, increment the counter
                    cont[j]++;    
                    }
                }          
            }
        }
        reader.close();
        return(cont);
    }

}
