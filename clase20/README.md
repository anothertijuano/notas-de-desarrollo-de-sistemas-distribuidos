# Clase 20

> Importante: Leer por completo todo el ejercicio antes de comenzar a programar. Cualquier duda favor de consultarla con el profesor.


Suponga que el servicio que brinda nuestro servidor en la nube consiste en generar 1757600 tokens aleatorios y hacer búsquedas de tokens de tres letras en dicha cadena. Supondremos que dicho servicio lleva tiempo funcionando y se sabe por estadísticas que en promedio se hacen cuatro solicitudes simultaneas al servidor la mayor parte del tiempo. Se nos plantean dos importantes preguntas: 

1. ¿Cuál CPU en Google Cloud Platform debemos elegir para tener la mejor relación costo-desempeño? Es decir que se desea pagar lo menos posible y que los usuarios obtengan los menores tiempos de respuesta en sus consultas.
2. ¿Cuál CPU debemos elegir si se desean los mínimos tiempos de respuesta sin importar el costo?

Para contestar estas preguntas tendrán que elaborar y entregar una tabla comparativa dentro de un documento Word con las distintas configuraciones de las CPU más convenientes. En dicha tabla tabulará el tipo de máquina, costo por mes, costo por segundo (con nueve cifras significativas), el tiempo de respuesta promedio para cuatro transacciones simultáneas en segundos(con nueve cifras significativas) y otra(s) variables que considere necesarias para responder a las preguntas 1 y 2.

Debido a que en principio se buscan los menores costos y considerando que cada proceso requiere un núcleo de CPU para ejecutarse se han elegido los siguientes candidatos:

Serie N1

Tipo de máquina f1-micro Intel Skylake

Tipo de maquina: n1-standard-1 (1CPU virtuales, 3.75GB de memoria)

Tipo de maquina: n1-standard-2 (2CPU virtuales, 7.5GB de memoria)

Tipo de maquina: n1-standard-4 (4CPU virtuales, 15GB de memoria)

Tipo de maquina: n1-highcpu-2 (2CPU virtuales, 1.8GB de memoria)

Tipo de maquina: n1-highcpu-4 (4CPU virtuales, 3.6GB de memoria)

Serie E2

Tipo de maquina: e2-micro (2CPU virtuales, 1GB de memoria)

Tipo de maquina: e2-highcpu-2 (2CPU virtuales, 2GB de memoria)

Tipo de maquina: e2-highcpu-4 (4CPU virtuales, 4GB de memoria)

Para ejecutar cuatro transacciones simultáneas es necesario ejecutar cuatro sesiones de curl exactamente al mismo tiempo y para lograrlo deberá hacer uso de hilos así como del método de Java exec( ) por lo que se recomienda revisar las siguientes ligas:

https://www.geeksforgeeks.org/multithreading-in-java/

 https://stackoverflow.com/questions/5711084/java-runtime-getruntime-getting-output-from-executing-a-command-line-program

TIPS:


	
Se recomienda ejecutar varias veces los clientes hasta que el tiempo de respuesta tienda a estabilizarse.
	
Dentro del código Java y en el arreglo de cadenas para ejecutar comandos no es necesario incluir las comillas simples. Ejemplo:


String[] commands = {"curl", "-v", "--header", "X-Debug:true", "--data", "1757600,IPN", "34.125.187.43:80/searchipn"};  


	
En lugar de instalar en cada instancia el jdk y el editor de textos, instalar el jre sin soporte de mouse, teclado y display, lo cual es más conveniente para servidores:


`sudo apt-get update`

`apt-cache search java`

`sudo apt-get install openjdk-17-jre-headless`

Así sólo es necesario subir el archivo class y ejecutarlo con el comando java. Busque en internet las distintas formas de subir el archivo y use la que más le convenga.


