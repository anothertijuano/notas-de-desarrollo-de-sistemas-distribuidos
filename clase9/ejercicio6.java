import java.util.*;
import java.util.Arrays;

class main6 {
    public static void main(String[] args) {
        long t0 = System.nanoTime();
        int n = Integer.parseInt(args[0]);
        Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
        Random r = new Random();
        StringBuilder str = new StringBuilder();
        for (int i=0; i<(n*4); i=i+4) {
            // Solo mayúsculas (65 al 90) = 26 letras, no incluye la Ñ
            StringBuilder s = new StringBuilder();
            s.append((char) (65 + r.nextInt(26)));
            s.append((char) (65 + r.nextInt(26)));
            s.append((char) (65 + r.nextInt(26)));
            s.append(" ");
            str.append(s);
            if(ht.containsKey(s.toString())) {
                int x = ht.get(s.toString());
                x = x+1;
                ht.put(s.toString(), x);
            } else {
                ht.put(s.toString(), 1);
            }
        }
        //String string = new String(cadenota);
        //System.out.println(str);
        //long t1 = System.nanoTime();
        //long tiempo = t1 - t0;
        //System.out.println("Nanosegundos: " + tiempo);
        System.out.println("Número de palabras únicas: "+ht.size());
        int sum = 0;
        Enumeration<String> e = ht.keys();
        while (e.hasMoreElements()) {
            String key = e.nextElement();
            sum = sum+ht.get(key);
        }
        System.out.println("Número de veces que se repite cada palabra (promedio): " + sum/ht.size());
    }
}

