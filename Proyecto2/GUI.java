// Proyecto 2
// David Andrade Olvera
// 4CM11

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.Console;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class GUI {

    public static void main(String[] args) {
        int nPoligonos = Integer.parseInt(args[0]);
        List<PoligonoReg> poligonos = new ArrayList<PoligonoReg>();
        Random rand = new Random();
        for(int i=0; i<nPoligonos; i++) {
            poligonos.add(new PoligonoReg(rand.nextInt(3, 15), rand.nextDouble()* (100 - 25) + 25));
        }
        System.out.println("Imprimiendo Polignos no ordenados...");
        new GUI(poligonos, false);
        try {
            Thread.sleep(3000);
        }
        catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        
        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.println("Imprimiendo Polignos ordenados...");
        
        //System.out.println("\nDesordenados:\n");
        //System.out.println(poligonos);
        Collections.sort(poligonos);
        List<PoligonoReg> tmp = new ArrayList<PoligonoReg>();
        
        for(int i=0; i<(nPoligonos); i++) {
            tmp.add(poligonos.get(i));
            try {
                Thread.sleep(1000);
            }
            catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            new GUI(tmp, true);
            if(!tmp.isEmpty()) {
                tmp.remove(0);
            }
        }

        
        
        //System.out.println("\nOrdenados:\n");
        //System.out.println(poligonos);
    }

    public GUI(List<PoligonoReg> poligonos, Boolean ordered) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                }

                JFrame frame = new JFrame("Proyecto 2 - DSD");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLayout(new BorderLayout());
                frame.add(new Panel(poligonos, ordered));
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

}
