// Proyecto 2
// David Andrade Olvera
// 4CM11

import java.lang.Math;

public class Coordenada implements Comparable<Coordenada>{

    private double x, y, d;

    public Coordenada(double x, double y) {
        this.x = x;
        this.y = y;
        this.d = Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2));
    }

    //Método getter de x
    public double abcisa( ) { return x; }



    //Método getter de y
    public double ordenada( ) { return y; }

    public double distancia( ) { return d; }


    //Sobreescritura del método de la superclase objeto para imprimir con System.out.println( )
    @Override
    public String toString( ) {
        return "[" + x + "," + y + "]"+": "+d;
    }

    //Sobreescribimos el metodo compareTo
    @Override
    public int compareTo(Coordenada x)
    {
        return Double.valueOf(this.distancia()).compareTo(x.distancia());
    }

}

