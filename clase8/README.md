# Clase 8

## Ejercicios de programación:

1.  Se ha elaborado el siguiente programa para convertir grados centígrados a Fahrenheit. Determine cual es el error y corríjalo mediante un cast para que dé el valor correcto.

```
public class EjerciciosSerie1 {

    public static void main(String[] args){

        double c = 20;

        double f;



        f = (9/5) * c + 32.0;

        System.out.println(f);

}
```

Si ejecutamos el programa nos da un resultado incorrecto de $52.0$ esto se debe a 
que la división de $9/5$ se trunca a $1$. Por lo que es necesario hacer un cast:

```
f = ((double)9/5) * c + 32.0;
```
