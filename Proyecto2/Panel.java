// Proyecto 2
// David Andrade Olvera
// 4CM11

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Panel extends JPanel {

    private List<PoliShape> poligonos = new ArrayList<PoliShape>();
    private Boolean ordered = false;

    public Panel(List<PoligonoReg> polis, Boolean ordered) {
        this.ordered = ordered;
        for(PoligonoReg poli: polis) {
            poligonos.add(new PoliShape(poli));
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1000, 1000);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        Random rand = new Random();
        if(ordered) {
            // Desplazar al centro de la pantalla
            g2d.translate(500, 500);
            // Color aleatorio
            g2d.setColor(new Color((int)(Math.random() * 0x1000000)));
            // Colorear poligono
            g2d.fill(poligonos.get(0));
            g2d.dispose();

        } else {

            // Desplazar al centro de la pantalla
            g2d.translate(500, 500);

            for(PoliShape poligono: poligonos) {
                // Color aleatorio
                g2d.setColor(new Color((int)(Math.random() * 0x1000000)));

                // Posición aleatoria
                int tx = rand.nextInt(-400, 400);
                int ty = rand.nextInt(-400, 400);
                g2d.translate(tx, ty);

                // Colorear poligono
                g2d.fill(poligono);

                // Regresar al centro
                g2d.translate(-tx, -ty);
            }
            g2d.dispose();
        }

    }
}