LECTURA 2
Due today at 8:30 AMCloses today at 11:59 PM
Instructions
Realizar en equipo y de manera distribuida, un organizador gráfico de información creativo con la información del libro:

“SISTEMAS DISTRIBUIDOS Principios y Paradigmas” de Andrew S. Tanenbaum , sobre los temas: 6 y 6.1  contenidos entre las páginas 231 y 242.



Con ayuda de una aplicación como Draw Chat

https://draw.chat/

u otra similar elaborar un organizador gráfico de la lectura donde participa todo el equipo de alumnos de manera concurrente. Dicho organizador tendrá que ser enviado por todos los miembros del equipo que trabajaron en su diseño a la plataforma de Microsoft teams y les será de apoyo para contestar con rapidez el examen teórico.



Por último, cada alumno deberá subir en la plataforma MOODLE un archivo de texto simple usando su matrícula de ESCOM como nombre del archivo y con extensión txt. El archivo debe estar elaborado en un editor de UNIX como vi, pico o nano y debe contener únicamente una pregunta de opción múltiple sobre la parte del texto que le tocó leer, cuidando las siguientes indicaciones:

•             La pregunta debe venir con cuatro opciones de respuesta.

•             La primera respuesta debe ser la correcta.

•             Cada respuesta va en una línea e inicia con letra mayúscula indicando el inciso, un punto y un espacio en blanco.

•             Evite preguntas con respuestas obvias.

•             Una buena pregunta tiene todas las opciones como lógicamente correctas y plausibles.

 

Ejemplo de pregunta:



¿Qué técnica se utiliza para que el procesador se ejecute a una velocidad de reloj más baja cuando las cargas del procesador son ligeras y funcionan a velocidades de reloj más altas cuando se incrementa la carga del procesador?

A. Processor throttling

B. Optimización de memoria

C. CPU filtering

D. Balanceo de carga


