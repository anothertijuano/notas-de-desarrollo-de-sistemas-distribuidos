# Sincronización

Si bien la comunicación entre procesos es importante, no lo es todo.
La cooperación es parcialmente soportada mediante la asignación de nombres.
Se necesitan nombre para compartir recursos o entidades.

Es importante que varios procesos no accedan simultáneamente a un recurso compartido, si no, que cooperen para garantizar el acceso exclusivo temporal al recurso.

Existen dos mecanismos de sincronización:
- Sincronización basada en tiempo real.
- Sincronización relativa al ordenamiento (tiempo absoluto).


## Relojes físicos

Un reloj de computadora es un componente que bajo condiciones especificas genera una señal eléctrica con una frecuencia específica que que, con apoyo de un registro mantenedor y un contador pueden llevar registro del tiempo transcurrido.

Usualmente cada sistema computacional cuenta con un reloj y una memoria en donde almacena un registro de la fecha y hora del sistema.

En los sistema distribuidos se cuenta con n  relojes presentes, donde inevitablemente los relojes tendrán valores distintos.

A la diferencia de valor entre relojes se le conoce como distorsión de reloj.

### ¿Qué es un segundo?

Existen diferentes tipos de "segundo" dependiendo del sistema que se tome como referencia para medirlos.

#### Segundo Solar

El segundo solar se define como 1/86400 el tiempo que transcurre entre el momento en el que el sol se encuentra a mitad de la trayectoria solar.

#### Segundo medio solar

El segundo medio solar es la media de multiples mediciones del segundo solar.

#### Segundo atómico

Se define un segundo como tiempo que le lleva a un átomo de cesio 133 efectuar 9192631770 transiciones.

#### Segundo universal coordinado

De acuerdo al Tiempo Universal Coordinado un segundo se basa en el segundo atómico, agregando segundos vacíos para compensar la diferencia entre un día atómico y un día solar.


### ¿Quién tiene el tiempo correcto?

Existen sistemas que se encargan de transmitir el valor del tiempo.

La estación de radio de onda corta WWB en Fort Collins Colorado emite un pulso corto al inicio de cada segundo UTC.

En Inglaterra la MSF de Rugby Warwickshire proporciona un servicio similar a la WWV.

E Geostationary Enviroment Operational Satellite proporciona el tiempo UTC con una precisión de 0.5ms. 
Claro es que para calcular la diferencia de tiempo desde que se transmite un valor hasta que lo recibe un sistema de cómputo depende de su posición geográfica y para ello se hace uso del sistema GPS.

Las empresas proveedoras de energía eléctrica enlazan las frecuencias de operación con el tiempo UTC.

### ¿Cómo nos ponemos de acuerdo?

Existen algoritmos de sincronización, que nos permiten ajustar el valor de multiples relojes en una red.

Todos los algoritmos suponen ca cada máquina cuenta con un reloj que ocasiona una interrupción X veces por segundo.

#### NTP

El protocolo de tiempo en red es un protocolo pasivo y está diseñado para configurar pares de sistemas.
En este protocolo el sistema A pregunta al sistema B por la hora, enviando la hora actual del sistema, con el objetivo de estimar la el retraso de reloj
$\delta = {{(T_2 -T_1) + (T_4 - T_3)} \over {2}}$. 

#### Berkely

EL algoritmo de Berkely es un algoritmo activo relativo; 
Relativo porque no importa la hora "correcta" mientras la hora entre los sistemas presentes en la red sea la misma.
Activo porque de manera constante consulta a los sistemas presentes por la hora con la que cuentan y ordena ajuste a sus respectivos relojes.

### Sincronización de transmisión de referencia (RBS)

En redes inalambricas se presentan nuevos retos de sincronización de dipositivos, no solo la mayoría de las máquinas pueden conectarse entre si, ahora se debe tomar en consideración 
cuestiones de eficiencia en el consumo de energía.

En RBS, no se asume que existe un solo sistema con la hora "exacta", en su lugar un sistema transmite un mensaje de referencia.







