import java.util.Random;

public class PruebaPoligono {

    public static void main (String[] args) {

        int n = Integer.parseInt(args[0]);
        Random rand = new Random();
        double vert[] = new double[2*n];

        long t0 = System.nanoTime();
        for(int i=0; i<(2*n); i=i+2) {
            vert[i] = rand.nextDouble();
            vert[i+1] = rand.nextDouble();
        }

        PoligonoIrreg pol1 = new PoligonoIrreg(n, vert);
        
        long t1 = System.nanoTime();
        long tiempo = t1 - t0;
        System.out.println("Nanosegundos: " + tiempo);
        System.out.println(pol1);


    }

}

