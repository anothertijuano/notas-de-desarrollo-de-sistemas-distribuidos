
import java.io.*;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Arrays;

public class Coordinador {

    public static void main(String []args) throws IOException, UnknownHostException {
        String[] IP ={args[0], args[1], args[2]};
        String[] palabras = Arrays.copyOfRange(args, 3, args.length);

        String[] array1 = Arrays.copyOfRange(palabras, 0, (palabras.length/3));
        String[] array2 = Arrays.copyOfRange(palabras, (palabras.length/3), (palabras.length/3)*2);
        String[] array3 = Arrays.copyOfRange(palabras, ((palabras.length/3)*2), palabras.length);

        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
        System.out.println(Arrays.toString(array3));
        
        try {
            httpGetRequest(args[0], array1);
        } catch (URISyntaxException | InterruptedException e) {
            e.printStackTrace();
        }
        try {
            httpGetRequest(args[1], array2);
        } catch (URISyntaxException | InterruptedException e) {
            e.printStackTrace();
        }
        try {
            httpGetRequest(args[2], array3);
        } catch (URISyntaxException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void httpGetRequest(String servidor, String []palabras) throws URISyntaxException, IOException, InterruptedException {
        String URL = "http://"+servidor+"/busqueda?";
        for(String palabra: palabras) {
            URL += "palabra="+palabra+"&";
        }
        
        
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .uri(URI.create(URL))
            .headers("Accept-Enconding", "gzip, deflate")
            .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

        String responseBody = response.body();
        //int responseStatusCode = response.statusCode();

        System.out.println(responseBody);
    }
    
}
