import java.util.Arrays;

public class PoligonoIrreg {

    // Arreglo simple de objetos Coordenada
    private Coordenada[] vertices;
    private int index = 0;


    public PoligonoIrreg(int n, double[] verti){
        // Inicializar arreglo
        vertices = new Coordenada[n];
        for(int i=0; i<(2*n); i=i+2) {
            // Setter
            anadeVertice(new Coordenada(verti[i], verti[i+1]));
        }
    }



    //Método setter de la coordenada superior izquierda
    public void anadeVertice(Coordenada x) {
        vertices[index++] = x;        
    }


    //Sobre escritura del método de la superClase objeto para imprimir con System.out.println( )
    @Override
    public String toString() {
        String str = "";
        for(int i=0; i<vertices.length; i++) {
            str += "Vertice"+i+" : " + vertices[i] +"\n";
        }
        return str;
    }

}

