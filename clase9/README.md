# CLASE 9
> [7:04 AM] Ukranio Coronilla Contreras

### 5 

El programa consiste en generar n “palabras” de tres letras mayúsculas aleatorias e irlas concatenando en una cadena gigante, manteniendo un espacio en blanco de separación entre cada palabra. Posteriormente debe hacerse la búsqueda de la subcadena “IPN” en la cadena gigante y contabilizar el número de apariciones, así como la posición dentro de la cadenota donde apareció.

El programa debe recibir el número n como parámetro en la línea de comandos de manera que debe incluir al inicio algo como:

```
n = Integer.parseInt(args[0]);
```

Por cuestiones de desempeño la cadena deberá almacenarse en un arreglo de tipo byte:

```
byte[] cadenota = new byte[n*4];
```

**¿De acuerdo con la teoría de la probabilidad cuantas palabras deberían generarse para que se dé una ocurrencia?** 

Si utilizamos un conjunto de [A-Z] sin incluir la letra Ñ, tenemos 26 elementos distintos que al poderse repetir en una palabra de 3 letras, se formarían 26\*26\*26 posibles palabras únicas.

**¿Y para diez ocurrencias?**

Para que se repitiera en promedio 10 veces cada palabra se tendrían que solicitar 26³\*10 palabras.



### 6
Elabore un programa similar al anterior, pero utilizando la clase StringBuilder y el método append para almacenar la cadena grande, así como el método indexOf para buscar la subcadena “IPN”. Posteriormente y haciendo uso del método System.nanoTime() compare los tiempos de ejecución del ejercicio anterior con el actual haciendo las búsquedas en una cadena suficientemente grande donde al menos en uno de los dos casos el tiempo sea aproximadamente de un segundo. 

Importante: Su código no debe imprimir nada, sólo hacer los cálculos, de lo contrario estará midiendo los tiempos de impresión más que los de procesamiento. Si es necesario optimice ambos códigos lo mejor posible.

¿Cuál de los dos códigos resultó ser más eficiente? ¿Cuántas veces resultó más rápido uno que el otro? ¿Qué he aprendido sobre los arreglos de char y la clase String? 

