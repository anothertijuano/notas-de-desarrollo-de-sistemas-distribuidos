public class ejercicio3 {
    public static void main(String[] args) {
        int start = 1;
        int end = 15;
        int steps = 2;

        for (int i=start; i<=end; i++) {
            System.out.printf("%d\n", i);
        }

        for (int i=end; i>=start; i=i-steps) {
            System.out.printf("%d\n", i);
        }
    }
}
