// Proyecto 4
// David Andrade
// 4CM11


import java.io.*;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

// Read the full article https://dev.to/mateuszjarzyna/build-your-own-http-server-in-java-in-less-than-one-hour-only-get-method-2k02
public class Server {

    // Función principal
    public static void main( String[] args ) throws Exception {
        // Primer argumento es puerto
        int port = Integer.parseInt(args[0]);

        // Siguientes 4 argumentos son IP:PORT
        ArrayList<String[]> servers = new ArrayList<String[]>();
        
        for(int i=1; i<args.length; i++) {
            servers.add(args[i].split(":",2));
        }

        // Creamos ServerSocket y esperamos nuevas conexiones
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                try (Socket client = serverSocket.accept()) {
                    // Atendemos petición
                    atenderCliente(client, servers);
                }
            }
        }
    }

    private static int atenderCliente(Socket client, ArrayList<String[]> servers) throws IOException, InterruptedException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));

        StringBuilder requestBuilder = new StringBuilder();
        String line;
        while (!(line = br.readLine()).isBlank()) {
            requestBuilder.append(line + "\r\n");
        }

        String request = requestBuilder.toString();
        String[] requestsLines = request.split("\r\n");
        String[] requestLine = requestsLines[0].split(" ");
        String path = requestLine[1];

        List<String> headers = new ArrayList<>();
        for (int h = 2; h < requestsLines.length; h++) {
            String header = requestsLines[h];
            headers.add(header);
        }

        String[] URL = path.split("\\?");

        String accessLog = String.format("Timestamp: %s", URL[1]);
        System.out.println(accessLog);

        if(!URL[0].equals("/time")) {
            System.out.println("Not path");
            sendResponse(client, "404 Not Found", "text/html; charset=UTF-8", "Not Found".getBytes());
            System.out.println();
            return(404);
        }

        

        
        // Capturamos el tiempo en que se atiende la petición
        long t0 = Instant.now().getEpochSecond();
        // Extraemos el tiempo de la petición
        long tc = Long.parseLong(URL[1]);
        // Obtenemos el tiempo promedio
        long tp = (t0+tc)/2;

        String json = new String();
        json = String.format("%d", tp);
        sendResponse(client, "200 OK", "application/json", json.getBytes());
        TimeUnit.SECONDS.sleep(2);
        sendRequest(servers, t0, tp);

        System.out.println();
        return(200);
        
    }

    private static void sendRequest(ArrayList<String[]> servers, Long t0, Long tp) throws IOException {
        Long t;
        for (String[] server: servers) {
            try {
                 // Agregamos la diferencia de tiempo entre que se recibío la petición y se genera la respuesta
                long tn = Instant.now().getEpochSecond();
                t = tp+(tn-t0);

                URL url = new URL(String.format("http://%s:%s/time?%d",server[0], server[1], t));
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Content-Type", "text/html");
                con.setConnectTimeout(500);
                con.setReadTimeout(500);
                con.setDoOutput(true);
                DataOutputStream out = new DataOutputStream(con.getOutputStream());
                out.flush();
                out.close();
                int status = con.getResponseCode();
                con.disconnect();  
                if (status == 200) {
                    break;
                }
            } catch(Exception e) {
                System.out.println(String.format("%s Down", server[0]));
            }
             
                            
        }

        

    }

    private static void sendResponse(Socket client, String status, String contentType, byte[] content) throws IOException {
        try {
            OutputStream clientOutput = client.getOutputStream();
            clientOutput.write(("HTTP/1.1 " + status).getBytes());
            clientOutput.write(("ContentType: " + contentType + "\r\n").getBytes());
            clientOutput.write("\r\n".getBytes());
            clientOutput.write(content);
            clientOutput.write("\r\n\r\n".getBytes());
            clientOutput.flush();
            client.close();

        } catch(Exception e) {}  
    }


}
