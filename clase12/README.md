# Clase 12

## 10

Retome la solución del ejercicio 8 y modifíquelo para que la clase PoligonoIrreg almacene los vértices en una interfaz List (véase [geaksforgeaks](https://www.geeksforgeeks.org/list-interface-java-examples/) ). Asimismo, dentro del del método toString haga uso de un bucle for-each para imprimir los elementos de List.

## 11

Incluya en la clase PoligonoIrreg el método ordenaVertices para ordenar los vértices de menor a mayor en función de su magnitud (distancia de la coordenada al origen) con ayuda del método Collections.sort (véase [geeksforgeeks](https://www.geeksforgeeks.org/collections-sort-java-examples/) ). Se recomienda agregar al objeto coordenada un nuevo miembro magnitud para facilitar el ordenamiento.

En el método principal instancie un objeto PoligonoIrreg al cual posea 10 vértices, todos ellos con valores reales aleatorios tanto positivos como negativos comprendidos entre -100 y 100. Imprima los vértices del polígono, mande a llamar el método de ordenamiento y posteriormente vuelva a imprimir los vértices del polígono.

