// Proyecto 2
// David Andrade Olvera
// 4CM11

import java.util.*;
import java.lang.Math;

public class PoligonoReg implements Comparable<PoligonoReg>{

    /* 
    Atributos
    */
    int nVertices;
    double alpha;
    double r;
    

    /* 
    Métodos
    */

    // Init
    public PoligonoReg(int nVertices, double r){
        this.nVertices = nVertices;
        this.r = r;
        this.alpha = 360.0/nVertices;
    }

    // Obtener área de poligono
    public double obtieneArea() {
        double area = this.nVertices*Math.pow(this.r, 2)*Math.sin(Math.toRadians(180/nVertices))*Math.cos(Math.toRadians(180/nVertices));
        //System.out.println(area);
        return(area);
    }

    public int getNvertices() {
        return(this.nVertices);
    }

    // Obtner coordenadas
    public Coordenada[] coordenadas() {
        Coordenada[] coordenadas = new Coordenada[this.nVertices];
        for(int i=0; i<this.nVertices; i++) {
            Coordenada tmp = new Coordenada((this.r*Math.cos(Math.toRadians(i*this.alpha))),(this.r*Math.sin(Math.toRadians(i*this.alpha))));
            coordenadas[i] = tmp;
            //System.out.println(String.format("(%f, %f)", tmp.abcisa(), tmp.ordenada()));
        }
        //System.out.println();
        return(coordenadas);
    }

    // Converit Poligono a string
    @Override
    public String toString() {
        return(String.format("Vertices:%d\tr: %f", this.nVertices, this.r));
    }

    //Sobreescribimos el metodo compareTo
    @Override
    public int compareTo(PoligonoReg x)
    {
        return Double.valueOf(this.obtieneArea()).compareTo(x.obtieneArea());
    }

}

